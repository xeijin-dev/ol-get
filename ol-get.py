# adapted from: https://stackoverflow.com/questions/21477599/read-outlook-events-via-python

import argparse
import win32com.client
import datetime
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse

# create command line arguments
parser = argparse.ArgumentParser(description='Export calendar entries from an outlook mailbox')
# parser.add_argument("-p", "--OlProfile", help="specify an outlook profile to load (default profile is used otherwise)")
# parser.add_argument("-c", "--Calendar", help="specify outlook calendar (folder name) to use (default calendar is used otherwise)")
parser.add_argument("-s", "--StartDate", type=str, help="the start date of the calendar appointment")
parser.add_argument("-e", "--EndDate", type=str, help="the end date of the calendar appointment")
parser.add_argument("-d", "--Duration", type=int, help="retrieve the next N days from StartDate (StartDate defaults to today if ommitted)")
# parser.add_argument("-j", "--OutputJson", help="output the results as JSON")
parser.set_defaults(func=lambda x: parser.print_usage())
args = parser.parse_args()

# process arguments
if args.StartDate is None:
    begin = datetime.date.today()
else:
    begin = parse(args.StartDate)

if (args.Duration is not None) and (args.EndDate is None):
    end = begin + datetime.timedelta(days=args.Duration)
else:
    end = parse(args.EndDate)

# initialise outlook COM object
outlook = win32com.client.Dispatch("Outlook.Application")
namespace = outlook.GetNamespace("MAPI")

appointments = namespace.GetDefaultFolder(9).Items # .GetDefaultFolder(9).Items = default calendar
appointments.Sort("[Start]")
appointments.IncludeRecurrences = "True"

# Get the AppointmentItem objects
# http://msdn.microsoft.com/en-us/library/office/aa210899(v=office.11).aspx

# Restrict to items in the next 30 days
restriction = "[Start] >= '" + begin.strftime("%m/%d/%Y") + "' AND [End] <= '" + end.strftime("%m/%d/%Y") + "'"
restrictedItems = appointments.Restrict(restriction)

# Iterate through restricted AppointmentItems and print them as a quoted emacs-lisp alist structure
alist = []
sep = ' '
for appointmentItem in restrictedItems:
    alist.append('((subject . "{0}") (start . "{1}") (end . "{2}") (organizer . "{3}") (eventId . "{4}"))'.format(
    appointmentItem.Subject.replace('"', "'"),
    appointmentItem.Start.isoformat(), #.strftime("%d-%m-%Y"),
    appointmentItem.End.isoformat(),
    appointmentItem.Organizer.replace('"', "'"),
    appointmentItem.EntryId))
print("(" + sep.join(alist) + ")")
