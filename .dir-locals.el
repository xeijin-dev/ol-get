;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (project-main-org-file . "ol-get.org")
  (propertized-project-name
   #("ol-get" 0 6
     (face
      (:foreground "#23a33e" :background "#f7fcff"))))
  (project-anon-face
   (:foreground "#23a33e" :background "#f7fcff"))
  (project-fg-colour . "#f7fcff")
  (project-bg-colour . "#23a33e")
  (projectile-project-name . "ol-get")))
