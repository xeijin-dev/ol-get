;;; ol-get.el - retrieve calendar entries from a local Outlook profile

;; Copyright (C) 2020 xeijin

;; Author: xeijin <dev@xeijin.org>
;; Version: 0.7
;; Package-Requires (s ts seq url json virtualenvwrapper posframe parse-time)
;; Keywords: outlook, ol, get, ews, ost, pst, exchange, web, services, emacs, elisp, calendar
;; URL: https://gitlab.com/xeijin-dev/ol-get

;;; Commentary:
;;
;; ol-get retrieves outlook calendar entries from a local outlook profile (PST/OST)
;; using a python script, accompanied by a few elisp functions to insert the output
;; into an emacs diary file
;;
;;; Code:

(require 's)
(require 'ts)
(require 'seq)
(require 'url)
(require 'json)
(require 'virtualenvwrapper)
(require 'posframe)
(require 'parse-time)

(defgroup ol-get nil
  "retrieve calendar entries from a local Outlook profile"
  :group 'emacs)

(defcustom ol-get-venv-name "ol-get-py"
  "the name of the python virtualenv into which the script will be installed"
  :type 'string
  :group 'ol-get)

(defcustom ol-get-venv-location (or (when load-file-name (concat (file-name-directory load-file-name))) venv-location)
  "the folder where the python script's virtualenv will be created, default is a folder named `ol-get-py' in the load-file directory."
  :type 'string
  :group 'ol-get)

(defcustom ol-get-diary-entry-formats '((org . "[[outlook:%s][%s]]"))
  "the format of a diary entry - defaults are org-mode and diary entry"
  :type '(alist :key-type (symbol) :value-type (string))
  :group 'ol-get)

(defcustom ol-get-diary-entry-format 'org
  "the default diary entry format to use from `ol-get-diary-entry-formats'"
  :type 'symbol
  :group 'ol-get)

(defcustom ol-get-outlook-diary-file (concat (file-name-directory diary-file) "outlook-diary")
  "the path to the diary file where entries for retrieved outlook calendar items will be stored.
  the default is a file named 'outlook-diary stored in the same directory as the default `diary-file'

  you cannot use your default diary file here (as the contents would be overwritten) - you can include
  this file in your default diary file using the #include directive."

  :type '(file)
  :group 'ol-get)

(defcustom ol-get-python-executable (or (executable-find "python3") (executable-find "python"))
  "the location of python.exe (or python command if already on path)"
  :type 'string
  :group 'ol-get)

(defcustom ol-get-executable (executable-find "ol-get")
  "path to ol-get.exe - `ol-get/ensure-executable' will set its path automatically when called if this variable doesn't point to an existing file."
  :type 'string
  :group 'ol-get)


(defcustom ol-get-ews-dll nil
  "(Windows) the full path to the EWS library: `Microsoft.Exchange.WebServices.dll'"
  :type '(file :must-match t)
  :group 'ol-get)

(defcustom ol-get-ews-config nil
  "alist of email address and server (domain only) used to retrieve outlook data via Exchange Web Services (EWS)
   this avoids the need to use EWS autodiscover, making data retrieval faster.

   the first entry is always used as the default"
  :type '(alist :tag "ews config"
                :key-type (string :tag "email address")
                :value-type (string :tag "ews server (domain only)"))
  :group 'ol-get)

(defmacro ol-get/with-msg (start-msg &optional end-msg &rest body)
  "show posframe with MSG then execute body, remove MSG once complete"

  `(let ((start-pf)
         (end-pf))
     (unwind-protect
         (progn
           (setq start-pf (ol-get/show-msg ,start-msg))
           (sit-for 0.1)
           (progn
             ,@body)
           (ol-get/delete-msg start-pf)
           (when ,end-msg
             (setq end-pf (ol-get/show-msg ,end-msg))
             (sit-for 1.5)
             (ol-get/delete-msg end-pf)))
       (posframe-delete-all))))

(defun ol-get/show-msg (msg)
  "show message MSG using a posframe"
  (let ((pf-name (make-temp-name "ol-get-msg-")))

    (when (posframe-workable-p)
      (posframe-show pf-name
                     :string msg
                     ;;:min-width 40
                     ;;:min-height 3
                     :font "28"
                     :foreground-color "Black"
                     :background-color "Salmon"
                     :position (point))
      pf-name)))

(defalias 'ol-get/delete-msg #'posframe-delete "delete a posframe message")

(defun ol-get/call-process (type command &rest args)
  "Execute COMMAND (of TYPE 'ps (powershell) or 'py (python)) with ARGS, synchronously.

  ARGS must include the named powershell parameter using :KEYWORD notation, followed by its argument.

  Returns (STATUS . OUTPUT) when it is done, where STATUS is the returned error
  code of the process and OUTPUT is its stdout output.

  ARGS will be transformed and passed to powershell exactly as shown below

  'true or t = $true
  'false or nil = $false
  'null = $null
  (ts-now) = \"05 January 2020 17:43:23\"
  (ts-parse \"16 Dec 2019 7:00am\") = \"16 December 2019 07:00:00\"
  (ts-inc 'day 2 (ts-now)) = \"07 January 2020 17:43:23\" -- see the `ts' library for more examples
  \"string\" = \"string\"
  \"'string'\" = 'string'
  '$var = $var"

  (fset 'is-ps-var (lambda (v) ; predicate to parse powershell variables
                     (when (and (symbolp v) (s-prefix? "$" (symbol-name v))) t)))

  ;; create a function to process each arg as required
  (fset 'process-arg (lambda (a)
                       (pcase a
                         ;; turn keywords into strings & replace ':' with powershell's '-'
                         ((pred keywordp) (s-replace ":" "-" (symbol-name a)))
                         ;;((pred stringp) (format "%S" a))
                         ((pred is-ps-var) (symbol-name a))
                         ;; if we have predicate for ps-var, shouldnt need the variations below --
                         ;; ((or 't 'true) "$true")
                         ;; ((or 'nil 'false) "$false")
                         ;; ('null "$null")
                         ((or (pred integerp) (pred floatp)) (format "%s" a))
                         ;; transform `ts' into default powershell date format
                         ;; TODO add/correct-for time-zone specification (since we're dealing with calendar entries...)
                         ((pred ts-p) (ts-format "%d-%m-%Y %H:%M:%S" a))
                         (_ a))))

  (let* ((args (mapcar 'process-arg args)))

    (pcase type
      
      ('py ;; python script
       (push command args)
       (push "-m" args)
       (venv-with-virtualenv ol-get-venv-name
                             (read (with-temp-buffer
                                     (apply #'call-process "python" nil t nil args)
                                     (buffer-string)))))

      ((or 'ps _) ;; powershell script, or anything else
       (with-temp-buffer
         (cons (or (apply #'call-process command nil t nil args)
                   -1)
               (string-trim (buffer-string))))))))

;; code adapted from: https://gist.github.com/cpbotha/05e07dee7fd8243ba73339be186c0b88
(defun ol-get/dl-archive-extract-file (archive-name item-name command dir keep-relpath)
  "Extract ITEM-NAME from ARCHIVE-NAME using COMMAND. Save to
DIR. If KEEP-RELPATH, extract with relative path otherwise don't.

Note: a windows build of the unix `unzip' command, on your path, is currently a requirement (see MinGW/MSYS2/GNUToolsW32 etc.)

due to corruption issues, currently COMMAND is unused, see comments in definition."

  ;; if the file is a URL, download it to temporary-file-directory, then perform the extraction
  (when (string-match-p "\\b\\(\\(www\\.\\|\\(s?https?\\|ftp\\|file\\|gopher\\|nntp\\|news\\|telnet\\|wais\\|mailto\\|info\\):\\)\\(//[-a-z0-9_.]+:[0-9]*\\)?\\(?:[-a-z0-9_=#$@~%&*+\\/[:word:]!?:;.,]+([-a-z0-9_=#$@~%&*+\\/[:word:]!?:;.,]+[-a-z0-9_=#$@~%&*+\\/[:word:]]*)[-a-z0-9_=#$@~%&*+\\/[:word:]]\\|[-a-z0-9_=#$@~%&*+\\/[:word:]!?:;.,]+[-a-z0-9_=#$@~%&*+\\/[:word:]]\\)\\)" archive-name)
    (let ((archive-dl-temp (concat temporary-file-directory (file-name-nondirectory archive-name))))
      (if (url-copy-file archive-name archive-dl-temp t)
          (setq archive-name archive-dl-temp)
        (user-error "error downloading archive: %s\nyou may need to download and extract manually from: %S" archive-name archive-name))))

  (let* ((file-name (if keep-relpath
                        ;; remove the leading / from the file name to force
                        ;; expand-file-name to interpret its path as relative to dir
                        (if (string-match "\\`/" item-name)
                            (substring item-name 1)
                          item-name)
                      ;; by default just strip the path completely
                      (file-name-nondirectory item-name)))
         (output-file (expand-file-name file-name dir)
                      (output-dir (file-name-directory output-file)))

         ;; create the output directory (and its parents) if it does
         ;; not exist yet
         (unless (file-directory-p output-dir)
           (make-directory output-dir t))


         ;; getting corrupted exe files on windows with 'std-out' 'unzip -qq -c'
         ;; execute COMMAND, redirecting output to output-file
         ;; (apply #'call-process
         ;;        (car command)            ;program
         ;;        nil                      ;infile
         ;;        `(:file ,output-file)    ;destination
         ;;        nil                      ;display
         ;;        (append (cdr command) (list archive-name item-name))))

         ;; TODO fix dependency on unix unzip here -- but not sure there's anything available natively in windows?
         (ol-get/call-process "unzip" "-qq" archive-name item-name "-d" output-dir)
         (when (file-exists-p output-file)
           output-file))))


;; (defun ol-get/ensure-executable ()
;;   "Ensure the ol-get.exe executable is available, attempt to download if not, signal an error if that fails"

;;   (interactive)

;;   (let* ((ews-executable-in-path (executable-find "ol-get"))
;;          (ol-get-root-dir (file-name-directory (or load-file-name buffer-file-name)))
;;          (ol-get-executable-in-root (concat ol-get-root-dir "ol-get.exe")))

;;     (cond
;;      (ol-get-executable (when (file-exists-p ol-get-executable)
;;                                     ol-get-executable))
;;      (ews-executable-in-path (customize-set-variable 'ol-get-executable ews-executable-in-path))
;;      (ol-get-executable-in-root (when (file-exists-p ol-get-executable-in-root)
;;                                    (customize-set-variable 'ol-get-executable ol-get-executable-in-root)))
;;      (t (if (y-or-n-p "'ol-get.exe' not found -- attempt to download latest version?")
;;                          (let* ((appveyor-api-last-build-entry (aref ;; use the appveyor API to get the version info instead, as BinTray's API requires authentication & is rate-limited
;;                                                                 (alist-get 'builds
;;                                                                            (json-read-from-string
;;                                                                             (with-temp-buffer
;;                                                                               (url-insert-file-contents ; insert response without HTTP headers, i.e. the actual JSON object
;;                                                                                "https://ci.appveyor.com/api/projects/xeijin/ol-get/history?recordsNumber=1")
;;                                                                               (buffer-string)))) 0))
;;                                 (appveyor-latest-version (alist-get 'version appveyor-api-last-build-entry))
;;                                 (ews-download (format "https://dl.bintray.com/xeijin-dev/werk/ol-get-%s.zip" appveyor-latest-version)))
;;                            (customize-set-variable
;;                             'ol-get-executable (ol-get/dl-archive-extract-file ews-download "ol-get.exe" archive-zip-extract ol-get-root-dir nil)))
;;                        (error "failed to find or download ol-get.exe"))))))

(defun ol-get/pwsh-format-list-filter (proc output) ;; deprecated as JSON is more reliable way to transmit than format-list which is at mercy of host width
  "transform the OUTPUT produced by powershell command PROC into an alist cons'd key-value pairs.
   Note: the powershell command should its results through the Format-List cmdlet."

  (let ;; split output on newlines to get individual entries
      ((entries (s-lines output))
       (result))

    ;; map over each entry, splitting keys from values then transform resulting list into cons'd alist
    (mapcar #'map-pairs
            (dolist (e entries result)
              (s-with (s-split-up-to " : " e 1) strim)))))

(defmacro ol-get/make-with-json (json-string body)
  "take JSON-STRING, bind elements using `let-alist', then execute BODY with let-bound tokens.

  if JSON-STRING is an array, map over each element in the array using `seq-map'

  the key of each element in the alist is bound to a symbol of the same name prefixed with '.'
  (the token) which resolves to the corresponding alist value:

  (name . 'value) => .name => 'value

  tokens can be combined to access nested elements of an alist:

  (name (nested . 'value)) => .name.nested => 'value"

  `(let ((json (json-read-from-string ,json-string))
         (fn (lambda (alist)
               (let-alist alist
                 ,body))))

     (if (vectorp json)
         (seq-map fn json) ;; map over a json array
       (funcall fn json))))

(defun ol-get/parse-json-date (json-date-string &optional format-string)
  "parse DATE-STRING (unevaluated date from JSON object created by Powershell's `ConvertTo-Json') & return human-readable date string.
   optionally, format the returned string according to specification in FORMAT-STRING,
   otherwise defaults to the 'hour minute' variant in `org-time-stamp-formats'.

   this function expects dates in the format: /\\Date(<unix epoch>)/\\."

  (let* ((m (remove json-date-string
                    (s-match-strings-all
                     (rx (and "/Date(" (group (+ digit)) ")/"))
                     json-date-string)))
         ;; with timezone rx .. but never seems to appear in powershell Date() timestamps anyway?
         ;; (rx (and "/Date("
         ;;          (or (group (+ digit))
         ;;              (and (group (+ digit))
         ;;                   (optional "-" (group (+ digit))))) ")/"))
         (datetime (seconds-to-time
                    (/ (string-to-number (car m)) 1000))))

    (unless format-string
      (setq format-string (cdr org-time-stamp-formats)))

    (format-time-string format-string datetime)))

(defun ol-get/ews-validate-retrieve-params (&rest params)
  "process PARAMS, retrieving the defaults for any missing, enriching with additional parameters, then return the full list

     PARAMS are specified one after the other, in the for:

     :PARAM1 value1 :PARAM2 value2 ...

     for a full list of paremeters see the documentation for ol-get.exe

     dates can be passed using the ts library, see ol-get/call-process for more information"

  (fset 'in-ews-cfg (lambda (e) (map-elt ol-get-ews-config e)))

  (let* ((default-config (nth 0 ol-get-ews-config))
         (default-email (car default-config))
         (email (plist-get params :EmailAddress))
         (server (plist-get params :Server))
         (ws-dll (plist-get params :WebServicesDLL))
         (additional-params '((:CalendarOnly . "")
                              (:UseLocalTime . "")
                              (:DestinationId . "EntryId"))))

    ;; set defaults where appropriate
    (when (not email)
      (if default-email
          (setq
           email default-email
           params (plist-put params :EmailAddress default-email))
        (user-error "you must specify an :EmailAddress ... alternatively, M-x customize-variable 'ol-get-ews-config'")))

    (when (not server)
      (if (in-ews-cfg email)
          (setq params (plist-put params :Server (alist-get email ol-get-ews-config)))
        (message "no server found or provided for '%s', using autodiscover (slower).\nM-x customize-variable 'ol-get-ews-config' if possible" email)))

    (when (not ws-dll)
      (if ol-get-ews-dll
          (setq params (plist-put params :WebServicesDLL ol-get-ews-dll))
        (user-error "you must specify a :WebServicesDLL location, see M-x customize-variable 'ol-get-ews-dll'")))

    ;; add in additional parameters
    (pcase-dolist (`(,param . ,value) additional-params)
      (unless (plist-member params param)
        (plist-put params param value)))

    ;; versions of emacs <26.3 won't take a malformed plist for plist-put
    ;; delete any proxy empty string values we use as a workaround
    (delete* "" params)
    params))

(defun ol-get/diary-get-ol-entries (&optional diary-file-path &rest params)
  "synchronously retrieves calendar entries with 'ol-get.exe' according to the passed PARAMS and add to emacs diary file.

   Use DIARY-FILE (full path) if specified, otherwose ude the default diary file.

   PARAMS are specified one after the other, inthe form:

   :PARAM2 value1 :PARAM2 value2 ...

   dates can be passed using the ts library, see ol-get/call-process for more information."

  (let ((params (apply #'ol-get/ews-validate-retrieve-params params))
        (results (list)))

    (when (not diary-file-path)
      (setq diary-file-path ol-get-outlook-diary-file))

    (when (equal diary-file-path diary-file)
      (user-error "ol-get: can't use default diary file for outlook entries, contents would be overwritten"))

    (with-temp-file diary-file-path
      (ol-get/make-with-json
       (cdr (apply #'ol-get/call-process 'ps "ol-get" params))

       (let* ((.Start (ol-get/parse-json-date .Start "%k:%M"))
              (.End (ol-get/parse-json-date .End "%k:%M")))

         (insert
          (format-time-string "%b %e, %Y\n")
          (format
           "  %s-%s %s\n"
           .Start .End (format "[[outlook:%s][%s]]" .CleanGlobalObjectID .Subject))))))

    (message "ol-get: outlook calendar entries retrieved and added to diary '%s'" (file-name-nondirectory diary-file-path))))

;;  working!
;;
;; (ol-get/ol-calendar-entries
;;  :EmailAddress "me@thing.com"
;;  :WebServicesDLL "C:/blah/webservices"
;;  :StartDate (ts-parse "16 Dec 2019 7:00")
;;  :EndDate (ts-parse "17 Dec 2019 21:00")
;;  :CalendarOnly ""
;;  :OutputJson "")

(defmacro ol-get/with-calendar-events (sequence body)
  "bind elements of SEQUENCE (a sequence of calendar events) using `let-alist', then execute BODY with let-bound tokens."

  `(let ((fn (lambda (alist)
               (let-alist alist
                 ,body))))
     (pcase ,sequence
       ((or (seq (pred vectorp)) ;; events as alist within json array
            (seq (seq (pred consp)))) ;; events as alist within nested list
        (seq-map fn ,sequence))
       ((map 'subject) ;; a single event alist
        (funcall fn ,sequence))
       (_ (user-error "not a valid calendar events sequence")))))

(defun ol-get/events-to-diary (events diary-file-path)
  "create diary entries from the specified EVENTS (a sequence of events) in the diary file at DIARY-FILE-PATH"

  (save-window-excursion
      (ol-get/with-calendar-events events
                                   (let* ((.start-iso (parse-iso8601-time-string .start))
                                          (.diary-date (format-time-string "%b %e, %Y" .start-iso))
                                          (.start-time (format-time-string "%k:%M" .start-iso))
                                          (.end-time (format-time-string "%k:%M" (parse-iso8601-time-string .end)))
                                          (entry-format (cdr (assoc ol-get-diary-entry-format
                                                                    ol-get-diary-entry-formats)))
                                          (entry (concat .diary-date "\n"
                                                         (format "  %s-%s %s\n" .start-time .end-time
                                                                 (format entry-format .eventId .subject)))))
                                     ;; FIXME still cant get clean buffer save & kill without weird buffer issues?
                                     (with-temp-file ol-get-outlook-diary-file
                                       (delete-matching-lines (regexp-quote .eventId) (point-min) (buffer-size))
                                       (diary-make-entry ;; TODO insert all events under one day's heading rather than one day per event?
                                        entry nil diary-file-path)
                                         (delete-duplicate-lines (point-min) (buffer-size) nil nil nil t))))))
                                     ;; (when (equal buffer-file-name diary-file-path)
                                     ;;   (basic-save-buffer-1) ;; save without running hooks
                                     ;;   (kill-buffer (current-buffer)))))))

(defun ol-get/outlook-calendar (start end-or-d &optional diary-file-path)
  "retrieve outlook calendar events between START (a timestamp, see below) and END-OR-D (timestamp or integer duration)
   write the resulting events to DIARY-FILE-PATH

  warning: this function will OVERWRITE the contents of `diary-file-path',
  it's recommended to use a separate 'staging' diary file for this purpose
  when setting the variable `ol-get-outlook-diary-file'

 TIMESTAMP can be:

   - a string, parseable by the python dateParse() function)
   - a `ts' struct (see documentation for `ts' for more info)"

  (interactive)

  (let* ((venv-location ol-get-venv-location)
         (diary-file-path (or diary-file-path ol-get-outlook-diary-file))
         (ts-fmt-str "%d %b %Y %R")
         (args (list :s start))
         (start-fmt (if (ts-p start)
                      (ts-format ts-fmt-str start)
                      start))
         (end-or-d-fmt)
         (ol-events))

    (pcase end-or-d
      ((pred ts-p)
       (setq args (append args (list :e end-or-d))
             end-or-d-fmt (ts-format ts-fmt-str end-or-d)))

       ((pred integerp)
        (setq args (append args (list :d end-or-d))
              end-or-d-fmt (if (< end-or-d 0)
                               (number-to-string end-or-d)
                             (concat "+" (number-to-string end-or-d)))))

       ((pred stringp)
        (setq args (append args (list :e end-or-d))
              end-or-d-fmt end-or-d)))

    (ol-get/with-msg "please wait, retrieving calendar entries" nil
                     (setq ol-events (apply #'ol-get/call-process 'py "ol-get" args))
                     (unless ol-events
                       (user-error "ol-get: no outlook events found in calendar"))
                     (ol-get/events-to-diary ol-events diary-file-path))

    (if (integerp end-or-d)
        (message "ol-get: retrieved outlook entries for %s, %s days." start-fmt end-or-d-fmt)
      (message "ol-get: retrieved outlook entries between %s and %s" start-fmt end-or-d-fmt))))


(defun ol-get/outlook-today (&optional diary-file-path)
  "retrieve outlook calendar events for today, and append to the emacs diary file DIARY-FILE-PATH"

  (interactive) ;; required in order to bind to key

  (let ((diary-file-path (or diary-file-path ol-get-outlook-diary-file)))

    (ol-get/outlook-calendar (ts-apply :hour 00 :minute 00 :second 00 (ts-now)) 1 diary-file-path)))

(defun ol-get/install-py-script ()
  "create a virtualenv, copy script & install required python modules"
  (save-current-buffer
    (let* ((venv-location ol-get-venv-location)
           (make-venv-cmd (list (or ol-get-python-executable
                                    (executable-find "python3")
                                    (executable-find "python"))
                                "-m"
                                "venv"
                                (concat ol-get-venv-location "/" ol-get-venv-name))))

      ;; check for existing venv first
      (when (venv-is-valid ol-get-venv-name)
        (user-error "ol-get: detected existing installation for '%s' in '%s' - please remove before attempting to reinstall"
                              ol-get-venv-name
                              ol-get-venv-location))

      (ol-get/with-msg "ol-get: installing python script ..." nil
                       (message "ol-get: installing script to python virtualenv ...")
                       (let ((inhibit-message t))
                         (apply #'call-process (pop make-venv-cmd) nil nil nil make-venv-cmd))
                       (venv-get-candidates) ;; refresh list of virtualenvwrapper.el envs
                       (venv-with-virtualenv ol-get-venv-name
                                             (copy-file "../ol-get.py" "./ol-get.py" t) ;; copy script into venv
                                             (shell-command "pip install pywin32 python-dateutil")) ;; install script dependencies
      (message "ol-get: 'ol-get.py' installed to '%s'" (concat venv-location "/" ol-get-venv-name))))))



(defun ol-get/ensure-dependencies ()
  "installs the python script & dependencies in a virtualenv, if not already"

  (interactive)

  (let* ((venv-location ol-get-venv-location)
         (default-directory venv-location))

      (unless (and (file-exists-p (concat ol-get-venv-name "/"))
                   (venv-is-valid ol-get-venv-name)
                   (venv-with-virtualenv ol-get-venv-name
                                         (file-exists-p (or "Scripts/" "Lib/" "Include/"))))
        (ol-get/install-py-script))))

;; install the python script if not already installed
(with-demoted-errors "%s" (ol-get/ensure-dependencies))

(provide 'ol-get)

;;; ol-get.el ends here
