#requires -version 4

<#
.SYNOPSIS
Ol-Get - retrieve outlook entries via EWS or local OST/PST file

.DESCRIPTION
This cmdlet wraps a few different powershell scripts and functions
providing features to access both online (Exchange Web Services)
and offline (.ost/.pst) outlook data

Note that the [EWS] and [LOCAL] notation indicates a parameter that applies to that mode

.PARAMETER Local
[LOCAL] Retrieve data from an Outlook profile for the current user (i.e. a .pst or .ost file)

.PARAMETER OlProfile
[LOCAL] The name of the Outlook profile to load, if this switch is missing (i.e. no profile is provided) the user's default profile is used

.PARAMETER Properties
[LOCAL] List of properties/fields (as comma separated strings) to retrieve

.PARAMETER EWS
[EWS] Retrieve data from an Exchange Web Services (EWS) server

.PARAMETER EmailAddress
[EWS] The e-mail address of the mailbox, which will be checked. The script accepts piped objects from Get-Mailbox or Get-Recipient

.PARAMETER Server
[EWS] By default the script tries to retrieve the EWS endpoint via Autodiscover. If you want to run the script against a specific server, just provide the name in this parameter. Not the URL!

.PARAMETER CalendarOnly
[EWS] By default the script will enumerate all folders under RecoverableItemsRoot and Calendar,Inbox and Sent Items. If you want to limit to folders with type "IPF.Appointment" use this switch.

.PARAMETER AllItemProps
[EWS|LOCAL] The full set of all properties for each item will be loaded

.PARAMETER StartDate
[EWS|LOCAL] Filter by Start date of a single appointment. Note: Cannot be used to find recurring meetings!

.PARAMETER EndDate
[EWS|LOCAL] Filter by End date of a single appointment. Note: Cannot be used to find recurring meetings!

.PARAMETER WebServicesDLL
[EWS] Path to the Exchange Web Services DLL file (usually in C:/Program Files (x86)/Microsoft/Exchange/Web Services/x.x/<dll>.dll)

.INPUTS
None

.OUTPUTS Log File
The script log file stored in C:\Windows\Temp${1}.log

.NOTES
Version:        0.1
Author:         xeijin
Creation Date:  11-01-2020
Purpose/Change: Initial version

.EXAMPLE
Retrieve calendar items from an EWS server

Ol-Get -EWS -EmailAddress "first.last@company.org" -Server "ews.mail.company.org" -StartDate "16 December 2019 00:00:00" -EndDate "17 December 2019 00:00:00" -CalendarOnly
.EXAMPLE
Retrieve calendar items from an EWS server

Ol-Get -Local -StartDate "16 December 2019 00:00:00" -EndDate "17 December 2019 00:00:00" -Properties "Subject", "Start", "End", "Categories"
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

[CmdletBinding()]
Param (
## Local Params
    [Parameter(
        Mandatory=$false,
        Position=0)]
    [System.String]
    $OlProfile,

    [Parameter(
        Mandatory=$false,
        Position=1)]
    [System.Array]
    $Properties,

    [Parameter(
        Mandatory=$false,
        Position=2)]
    [System.DateTime]
    $StartDate,

    [Parameter(
        Mandatory=$false,
        Position=3)]
    [System.DateTime]
    $EndDate
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Get-CalendarItems (EWS fns)
#. . ".\Get-CalendarItems.ps1"

#Import Modules & Snap-ins
Import-Module PSLogging

#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Script Version
$sScriptVersion = '0.1'

#Log File Info
$sLogPath = 'C:\Windows\Temp'
$sLogName = 'ol-get.log'
$sLogFile = Join-Path -Path $sLogPath -ChildPath $sLogName

#-----------------------------------------------------------[Functions]------------------------------------------------------------



Function Get-OutlookCalendar {
    Param ()

    Begin {
        Write-LogInfo -LogPath $sLogFile -Message 'retrieving calendar items from local profile...'
    }

    Process {
        Try {
            # load the required .NET types
            Add-Type -AssemblyName 'Microsoft.Office.Interop.Outlook'

            # access Outlook object model
            $outlook = New-Object -ComObject outlook.application

            # TODO use the default OR specified profile
            $outlook.Session.Logon($outlook.DefaultProfile)

            # connect to the appropriate location
            $namespace = $outlook.GetNameSpace('MAPI')
            $Calendar = [Microsoft.Office.Interop.Outlook.OlDefaultFolders]::olFolderCalendar
            $folder = $namespace.getDefaultFolder($Calendar)
            # get calendar items
            $folder.items |
              Where-Object {[DateTime]::Parse($StartDate) -le [DateTime]::Parse($_.Start)} #-And [DateTime]::Parse($EndDate) -ge [DateTime]::Parse($_.End)}
              Select-Object -Property $Properties #"Subject", "Start", "End", "Organizer", "EntryId"
        }

        Catch {
            Write-LogError -LogPath $sLogFile -Message $_.Exception -ExitGracefully
            Break
        }
    }

    End {
        If ($?) {
            Write-LogInfo -LogPath $sLogFile -Message 'Completed Successfully.'
            Write-LogInfo -LogPath $sLogFile -Message ' '
        }
    }
}



#-----------------------------------------------------------[Execution]------------------------------------------------------------

Start-Log -LogPath $sLogPath -LogName $sLogName -ScriptVersion $sScriptVersion
Get-OutlookCalendar -Properties $Properties -StartDate $StartDate -EndDate $EndDate
Stop-Log -LogPath $sLogFile
